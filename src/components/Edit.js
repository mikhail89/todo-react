import React from "react";
import DatePicker from "react-datepicker";
import { format } from "date-fns";
import { Button, InputGroup, FormControl, Modal, Form } from "react-bootstrap";

class Edit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ...props.item,
      childVisible: false,
      startDate: null,
      show: true,
    };
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.item !== this.props.item) {
      console.log("componentDidUpdate");
      this.setState({
        task: this.props.item.task,
        description: this.props.item.description,
        deadline: this.props.item.deadline,
        done: this.props.item.done,
        priority: this.props.item.priority,
      });
    }
  };

  editOnClick = () => {
    this.setState((prevState) => ({
      childVisible: !prevState.childVisible,
    }));
  };

  dateTempChange = (handleChange) => {
    const arrDate = format(handleChange, "MM/dd/yyyy");
    this.setState({
      startDate: handleChange,
      deadline: arrDate,
    });
  };

  tempChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  onSubmit = (e, id) => {
    e.preventDefault();
    this.props.onSubmit(this.state);
    this.setState({
      childVisible: false,
      // startDate: null,
      // task: "",
      // description: "",
      // priority: "",
      // deadline: "",
      // done: "",
    });
  };

  render() {
    const { childVisible } = this.state;
    const { item } = this.props;

    const { id, ...withoutId } = item;

    const inputs = Object.keys(withoutId).map((field, i) => {
      if (
        field === "startDate" ||
        field === "childVisible" ||
        field === "show" ||
        field === "openAddTodo"
      ) {
        return false;
      } else if (field === "deadline") {
        return (
          <div>
            <InputGroup className="mb-3" style={{ width: "100%" }}>
              <InputGroup.Prepend>
                <InputGroup.Text
                  style={{
                    width: "235px",
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  {field}
                </InputGroup.Text>
              </InputGroup.Prepend>
              <DatePicker
                name="deadline"
                selected={this.state.startDate}
                onChange={(e) => this.dateTempChange(e)}
                value={this.state[field]}
                customInput={<FormControl style={{ width: "232px" }} />}
              ></DatePicker>
            </InputGroup>
          </div>
        );
      } else if (field === "done") {
        return (
          <Form.Group style={{ width: "100%", margin: "10px" }}>
            <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text
                  style={{
                    width: "235px",
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  Select "done" or "in progress"
                </InputGroup.Text>
              </InputGroup.Prepend>

              <Form.Control
                as="select"
                placeholder="done"
                defaultValue="in progress"
                key={i}
                name={field}
                onChange={(e) => this.tempChange(e)}
                value={this.state[field]}
              >
                <option>Choose</option>
                <option>Done</option>
                <option>In progress</option>
              </Form.Control>
            </InputGroup>
          </Form.Group>
        );
      } else {
        return (
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text
                style={{
                  width: "235px",
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                {field}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              key={i}
              name={field}
              onChange={(e) => this.tempChange(e)}
              value={this.state[field]}
            />
          </InputGroup>
        );
      }
    });

    return (
      <div>
        <Button
          style={{ margin: "5px", height: "52px" }}
          variant="info"
          onClick={this.editOnClick}
        >
          Edit todo
        </Button>

        <br />
        {childVisible && (
          <Modal show={this.state.show} onHide={this.editOnClick}>
            <Modal.Header closeButton>
              <Modal.Title>Fill in the fields</Modal.Title>
            </Modal.Header>
            <Modal.Body
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              {inputs}
              <Button
                style={{
                  width: "110px",
                  height: "52px",
                }}
                variant="primary"
                onClick={(e) => this.onSubmit(e)}
              >
                Submit
              </Button>
            </Modal.Body>

            <Modal.Footer>
              <Button variant="secondary" onClick={this.editOnClick}>
                Close
              </Button>
            </Modal.Footer>
          </Modal>
        )}
      </div>
    );
  }
}

export default Edit;
