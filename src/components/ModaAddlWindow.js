import { DatePickerInput } from "rc-datepicker";
import React, { useState } from "react";
import { Button, Form, FormControl, InputGroup, Modal } from "react-bootstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";


const ModaAddlWindow = (props) => {
  const [show, setShow] = useState(true);

  const handleClose = () => {
    setShow(false);
    props.changeStateOpenAddTodo();
  };

  return (
    <div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Fill in the fields</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <InputGroup style={{ width: "100%", margin: "10px" }}>
              <InputGroup.Prepend>
                <InputGroup.Text
                  style={{
                    width: "235px",
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  Task
                </InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                placeholder="task"
                name="task"
                value={props.task}
                onChange={(e) => props.change(e)}
                aria-label="Default"
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
            <InputGroup style={{ width: "100%", margin: "10px" }}>
              <InputGroup.Prepend>
                <InputGroup.Text
                  style={{
                    width: "235px",
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  Description
                </InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                placeholder="description"
                name="description"
                value={props.description}
                onChange={(e) => props.change(e)}
                aria-label="Default"
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
            <InputGroup style={{ width: "100%", margin: "10px 0" }}>
              <InputGroup.Prepend>
                <InputGroup.Text
                  style={{
                    width: "235px",
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  Priority (numerically)
                </InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                type="number"
                placeholder="priority"
                name="priority"
                value={props.priority}
                onChange={(e) => props.change(e)}
                aria-label="Default"
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
            <InputGroup style={{ width: "100%", margin: "10px" }}>
              <InputGroup.Prepend>
                <InputGroup.Text
                  style={{
                    width: "235px",
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  Deadline
                </InputGroup.Text>
              </InputGroup.Prepend>
              <DatePicker
                class="form-control"
                placeholderText="enter deadline"
                selected={props.startDate}
                onChange={(e) => props.onDateChange(e)}
                customInput={<FormControl style = {{width: '232px'}}/>}
              />
            </InputGroup>
            <br />
            <Form.Group style={{ width: "100%", margin: "10px" }}>
              <InputGroup>
                <InputGroup.Prepend>
                  <InputGroup.Text
                    style={{
                      width: "235px",
                      flex: 1,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    Select "done" or "in progress"
                  </InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                  as="select"
                  placeholder="done"
                  defaultValue="in progress"
                  name="done"
                  value={props.done}
                  onChange={(e) => props.change(e)}
                >
                  <option>Choose</option>
                  <option>Done</option>
                  <option>In progress</option>
                </Form.Control>
              </InputGroup>
            </Form.Group>

            <Button variant="success" onClick={(e) => props.onSubmit(e)}>
              Add todo
            </Button>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default ModaAddlWindow;

