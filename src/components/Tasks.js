import React from "react";
import { Button, Table } from "react-bootstrap";

class Tasks extends React.Component {
  render() {
    return (
      <div style={{ display: "flex" }}>
        <Table
          style={{ margin: "5px", width: "1000px" }}
          striped
          bordered
          hover
          variant="dark"
        >
          <thead>
            <tr>
              <th style={{ width: "100px" }}>Task: {this.props.task}</th>
              <th style={{ width: "100px" }}>
                Description: {this.props.description}
              </th>
              <th style={{ width: "100px" }}>
                Priority: {this.props.priority}
              </th>
              {new Date(this.props.deadline) -
                24 * 60 * 60 -
                (new Date() - 24 * 60 * 60) >
              259200000 ? (
                <th style={{ width: "100px", color: "white" }}>
                  Deadline: {this.props.deadline}
                </th>
              ) : (
                <th style={{ width: "100px", color: "red" }}>
                  Deadline: {this.props.deadline}
                </th>
              )}

              <th style={{ width: "100px" }}>Done: {this.props.done}</th>
            </tr>
          </thead>
        </Table>
        <Button
          style={{
            width: "110px",
            height: "52px",
            margin: "5px",
          }}
          variant="danger"
          onClick={() => this.props.delEvent(this.props.id)}
        >
          Delete todo
        </Button>
      </div>
    );
  }
}

export default Tasks;
