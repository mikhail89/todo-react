import React from "react";

import "react-datepicker/dist/react-datepicker.css";
import { format } from "date-fns";
import { Button} from "react-bootstrap";
import ModaAddlWindow from "./ModaAddlWindow";

class AddNewTask extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      startDate: null,
      task: "",
      description: "",
      priority: "",
      deadline: "",
      done: "",
      openAddTodo: false,
    };
  }

  openAddTodoClick = () => {
    this.setState((prevState) => ({
      openAddTodo: !prevState.openAddTodo,
    }));
  };
  onDateChange = (handleChange) => {
    const arrDate = format(handleChange, "MM/dd/yyyy");
    this.setState({
      startDate: handleChange,
      deadline: arrDate,
    });
  };

  change = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.props.onSubmit(this.state);
    this.setState((prevState) => ({
      startDate: null,
      task: "",
      description: "",
      priority: "",
      deadline: "",
      done: "",
      openAddTodo: !prevState.openAddTodo,
    }));
  };

  render() {
    const openAddTodo = this.state.openAddTodo;

    return (
      <div>
        <Button
          style={{
            width: "110px",
            height: "52px",
            margin: "20px",
          }}
          onClick={() => this.openAddTodoClick()}
        >
          Add task
        </Button>
        {openAddTodo && (
          <ModaAddlWindow
            description={this.state.description}
            priority={this.state.priority}
            startDate={this.state.startDate}
            done={this.state.done}
            changeStateOpenAddTodo={this.openAddTodoClick}
            change={this.change}
            onDateChange={this.onDateChange}
            onSubmit={this.onSubmit}
          ></ModaAddlWindow>
        )}
      </div>
    );
  }
}

export default AddNewTask;
