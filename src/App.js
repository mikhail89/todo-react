import React from "react";
import Tasks from "./components/Tasks";
import AddNewTask from "./components/AddNewTask";
import UniqueId from "react-html-id";
import SearchBox from "./components/SearchBox";
import Edit from "./components/Edit";
import "bootstrap/dist/css/bootstrap.min.css";

class App extends React.Component {
  constructor() {
    super();
    UniqueId.enableUniqueIds(this);
    this.state = {
      todolist: [
        {
          id: this.nextUniqueId(),
          task: "",
          description: "",
          priority: "",
          deadline: "",
          done: "",
        },
      ],
      childVisible: false,
      searchTask: "",
    };
  }

  // const [todolist, setTodolist] = useState([
  //   {
  //     id: this.nextUniqueId(),
  //     task: "",
  //     description: "",
  //     priority: "",
  //     deadline: "",
  //     done: "",
  //   },
  // ]

  editChange = (e, id) => {
    const index = this.state.todolist.findIndex((todo) => {
      return todo.id === id;
    });

    const todo = { ...e, id };

    const todolist = Object.assign([], this.state.todolist);

    todolist[index] = todo;

    this.setState({ todolist });
  };

  addTodo = (fields) => {
    const newTask = {
      ...fields,
      id: new Date(), //.nextUniqueId()
    };
    this.setState({ todolist: this.state.todolist.concat(newTask) });
  };

  deleteTodo = (id, e) => {

    const todolist = this.state.todolist.filter((item) => id !== item.id);
    this.setState({ todolist });
  };

  handleInput = (e) => {
    this.setState({ searchTask: e.target.value });
  };

  render() {
    let filteredTask = this.state.todolist.filter((itemlist) => {
      return (
        itemlist.task
          .toLowerCase()
          .includes(this.state.searchTask.toLowerCase()) ||
        itemlist.description
          .toLowerCase()
          .includes(this.state.searchTask.toLowerCase()) ||
        itemlist.priority
          .toLowerCase()
          .includes(this.state.searchTask.toLowerCase()) ||
        itemlist.deadline
          .toLowerCase()
          .includes(this.state.searchTask.toLowerCase())
      );
    });

    let todolist = filteredTask
      .sort((a, b) => a.priority - b.priority)
      .map((item) => {
        return (
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <div style={{ display: "flex", width: "100%" }}>
              <Tasks
                key={item}
                // task={item.task}
                // description={item.description}
                // priority={item.priority}
                // deadline={item.deadline}
                // done={item.done}
                {...item}
                id={item.id}
                delEvent={this.deleteTodo}
              />

              <Edit onSubmit={(e) => this.editChange(e, item.id)} item={item} />
            </div>
          </div>
        );
      });

    return (
      <div
        style={{
          width: "100%",
          height: "1000px",
          backgroundColor: "#87CEEB",
          textAlign: "center",
          padding: "20px",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
        className="App"
      >
        {todolist}
        <SearchBox handleInput={this.handleInput} />

        <div>
          <AddNewTask onSubmit={(fields) => this.addTodo(fields)} />
        </div>
      </div>
    );
  }
}

export default App;
